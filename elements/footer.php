<?php $option1 = false;
$option2 = true; ?>

<div class="container">
	<div class="row">
		<div class="col l6 s12">
			<div class="fb-page" data-href="https://www.facebook.com/kalibra" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
				<div class="fb-xfbml-parse-ignore">
				<blockquote cite="https://www.facebook.com/kalibra"><a href="https://www.facebook.com/kalibra">Kalibra Automação Ltda</a>
				</blockquote>
			</div>
		</div>
</div>
<div class="col l3 s12">
	<h5 class="white-text"><?php echo Text::PHONES; ?></h5>
	<ul>
		<li><a class="white-text waves-effect waves-light" href="#!"><i class="material-icons left">phone</i><?php echo $phones['telefone']; ?></a></li>
		<li><a class="white-text waves-effect waves-light" href="#!"><i class="material-icons left">phone</i><?php echo $phones['celular']; ?></a></li>
	</ul>
</div>
<div class="col l3 s12">
	<h5 class="white-text"><?php echo Text::CONNECT; ?></h5>
	<span class="footer-social-icon">
		<?php echo SocialMediaHelper::iconBar('facebook', $socialMedia['facebook']); ?>
		<?php //echo SocialMediaHelper::iconBar('twitter', ''); ?>
		<?php echo SocialMediaHelper::iconBar('google-plus', $socialMedia['google-plus']); ?>
	</span>
</div>
</div>
</div>
<div class="footer-copyright">
	<div class="container grey-text text-lighten-1">
		<?php echo Text::COPYRIGHT; ?>
		<span class="right footer-social-icon">
			<a href="#home" class="waves-effect waves-light teal-text tooltipped" data-position="top" data-delay="50" data-tooltip="Voltar ao início"><i class="material-icons">navigation</i></a>
			<?php //echo SocialMediaHelper::copyrightIconBar('facebook', $socialMedia['facebook'], 'Like us on Facebook'); ?>
			<?php //echo SocialMediaHelper::copyrightIconBar('twitter', '', 'Follow us on Twitter'); ?>
			<?php //echo SocialMediaHelper::copyrightIconBar('google-plus', $socialMedia['google-plus'], 'Follow us on Google Plus'); ?>
			<?php //echo SocialMediaHelper::copyrightIconBar('dribbble', '', 'Follow us on Google Plus'); ?>
			<?php //echo SocialMediaHelper::copyrightIconBar('youtube', '', 'Subscribe on Youtube'); ?>
		</span>
	</div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
