<?php

class FormHelper{
	static function input($label, $id, $type='text', $options=['class'=>'validate']){
		$classes = $options['class'];
		$html  = '<input id="'.$id.'" name="'.$id.'" type="'.$type.'" class="'.$classes.'">';
		$html .= '<label for="'.$id.'">'.$label.'</label>';

		return $html;
	}

	static function textarea($label, $id, $options=['class'=>'']){
		$classes = $options['class'];
		$html  = '<textarea id="'.$id.'" name="'.$id.'"  type="textarea" class="'.$classes.'"></textarea>';
		$html .= '<label for="'.$id.'">'.$label.'</label>';

		return $html;
	}
}

class SocialMediaHelper{
	static function facebook($address="#!"){
		$html  = '<a class="primaryColor" href="'.$address.'">';
		$html .= '<i class="fa fa-facebook"></i></a>';

		return $html;
	}

	static function twitter($address="#!"){
		$html  = '<a class="primaryColor" href="'.$address.'">';
		$html .= '<i class="fa fa-twitter"></i></a>';

		return $html;
	}

	static function googleplus($address="#!"){
		$html  = '<a class="primaryColor" href="'.$address.'">';
		$html .= '<i class="fa fa-google-plus"></i></a>';

		return $html;
	}

	static function instagram($address="#!"){
		$html  = '<a class="primaryColor" href="'.$address.'">';
		$html .= '<i class="fa fa-instagram"></i></a>';

		return $html;
	}

	static function iconBar($type, $address="#!"){
		$html = "";

		$html .= '<a href="'.$address.'" class="icon_bar  icon_bar_'.str_replace("-","_",$type).' icon_bar_small" target="_blank">';
		$html .= '<span class="t"><i class="fa fa-'.$type.'"></i></span>';
		$html .= '<span class="b"><i class="fa fa-'.$type.'"></i></span>';
		$html .= '</a>';


		return $html;
	}
	static function copyrightIconBar($type, $address="#!", $tooltip=""){
		$html = "";

		$html .= '<a data-position="top" data-delay="50" data-tooltip="'.$tooltip.'" class="grey-text text-lighten-1 tooltipped" href="'.$address.'">';
		$html .= '<i class="fa fa-'.$type.'"></i>';
		$html .= '</a>';


		return $html;
	}

}
