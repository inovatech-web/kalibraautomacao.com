<?php
$title = "Kalibra Automação e Balanças";

$address = ['rua'    => 'Rua Raul Dalvo Fontana',
			 'numero' => '88',
			 'cep'    => '95032-640',
			 'bairro' => 'Bairro Santa Catarina',
			 'cidade' => 'Caxias do Sul',
			 'uf'     => 'RS'
			];

$phones = ['telefone' => '(54) 3419-5723',
			  'celular'  => '(54) 9681-1905'];

$socialMedia = ['facebook' => 'https://www.facebook.com/kalibra' ,
               	'google-plus'  => 'https://www.facebook.com/kalibra' ,
               ];

$partners = [
		[
			'title' => 'Toledo',
			'class' => 'toledo',
			'url'  => '#',
	  ],
		[
			'title' => 'Urano',
			'class' => 'urano',
			'url'  => '#',
	  ],
		[
			'title' => 'Filizola',
			'class' => 'filizola',
			'url'  => '#',
	  ],
		[
			'title' => 'Daruma',
			'class' => 'daruma',
			'url'  => '#',
	  ],
		[
			'title' => 'Honeywell',
			'class' => 'honeywell',
			'url'  => '#',
	  ],
		[
			'title' => 'Gertec',
			'class' => 'gertec',
			'url'  => '#',
	  ],
		[
			'title' => 'Elgin',
			'class' => 'elgin',
			'url'  => '#',
	  ],
		[
			'title' => 'Bematech',
			'class' => 'bematech',
			'url'  => '#',
	  ],
		[
			'title' => 'Argox',
			'class' => 'argox',
			'url'  => '#',
	  ],
		[
			'title' => 'Elo Touch',
			'class' => 'elo-touch',
			'url'  => '#',
	  ],
		[
			'title' => 'Motorola',
			'class' => 'motorola',
			'url'  => '#',
	  ],
		[
			'title' => 'Digitron',
			'class' => 'digitron',
			'url'  => '#',
	  ],
		[
			'title' => 'Triunfo',
			'class' => 'triunfo',
			'url'  => '#',
	  ],
		[
			'title' => 'Alfa Instrumentos',
			'class' => 'alfa-instrumentos',
			'url'  => '#',
	  ],
            ];

class Text{
	const HOME = "Bem-Vindo";
	const ABOUT = "Sobre nós";
	const CONTACT = "Contato";
	const SERVICES = "Serviços";
	const PARTNERS = "Autorizadas";
	const PRODUCTS = "Produtos";
	const LOCATION = "Como chegar";

	const ADDRESS = "Endereço";
	const CONNECT = "Conecte";
	const PHONES = "Telefones";

	const REVENDA = "Revenda e Suporte";

	const HORARIO_ATENDIMENTO = "Horário de Atendimento";


	const COPYRIGHT = "2016 © Kalibra Automação Ltda";

	static function e($string){
		echo $string;
	}
}
