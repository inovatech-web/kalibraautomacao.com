<div class="row">
	<!-- Heading -->
	<div class="col xs12 m12 s12 l12">
		<div class="title-center wow bounceIn">
			<div class="bottom-line">
				<h2><?php echo Text::PARTNERS; ?></h2>
				<span class="line"><em></em></span>
			</div>
		</div>
		<div id="menu-partners">
	    <ul class="row grid">
	        <?php foreach ($partners as $partner) :?>
	        		<li class="grid-item <?php echo $partner['class']; ?>"><a href="#" title="<?php echo $partner['title']; ?>"></a></li>
				<?php endforeach;?>
	    </ul><!-- .inline-block -->
		</div>
	</div>
</div>
