<!-- CAROUSEL -->
	<div class="col xs12 m12 s12 l12">
		<div class="slider">
			<ul class="slides">
				<li>
					<img src="./resources/img/slides/bg_welcome.jpg"> <!-- random image -->
					<div class="caption center-align">
						<h3>Bem-vindo a Kalibra Automação Ltda</h3>
						<h5 class="light grey-text text-lighten-3">Serviços avulsos e Contratos de Manutenção</h5>
						<h5 class="light grey-text text-lighten-3" id="slide-logo-inmetro">Assistência Técnica Autorizada</h5>
						<a class="waves-effect waves-light btn" target="_blank" href="#contact">Entre em contato</a>
					</div>
					<div class="caption right-align">

					</div>
				</li>
				<li>
					<img src="./resources/img/slides/gestao3.jpg"> <!-- random image -->
					<div class="caption left-align">
						<h3>Sistema de Gestão</h3>
						<h5 class="light grey-text text-lighten-3">Cupom Fiscal e NF-e</h5>
						<h5 class="light grey-text text-lighten-3">Controle de clientes e fornecedores</h5>
						<h5 class="light grey-text text-lighten-3">Controle de produtos e estoque</h5>
						<h5 class="light grey-text text-lighten-3">Orçamentos e pedidos</h5>
						<a class="waves-effect waves-light btn" target="_blank" href="http://www.domper.com.br/site/solucoes_gestao.php">Saiba mais</a>
					</div>
				</li>
				<li>
					<img src="./resources/img/slides/restaurante2.jpg"> <!-- random image -->
					<div class="caption left-align">
						<h3>Sistema para Restaurante</h3>
						<h5 class="light grey-text text-lighten-3">Cupom Fiscal - TEF</h5>
						<h5 class="light grey-text text-lighten-3">Controle por mesa ou comanda</h5>
						<h5 class="light grey-text text-lighten-3">Delivery (tele-entrega)</h5>
						<h5 class="light grey-text text-lighten-3">Impressão de pedidos</h5>
						<a class="waves-effect waves-light btn" target="_blank" href="http://www.domper.com.br/site/solucoes_restaurante.php">Saiba mais</a>
					</div>
				</li>
				<li>
					<img src="./resources/img/slides/fruticultura2.jpg"> <!-- random image -->
					<div class="caption right-align">
						<h3>Sistema para Fruticultura</h3>
						<h5 class="light grey-text text-lighten-3">NF-e / Contabilidade Integrada</h5>
						<h5 class="light grey-text text-lighten-3">Produção e rastreabilidade</h5>
						<h5 class="light grey-text text-lighten-3">Controle de qualidade</h5>
						<h5 class="light grey-text text-lighten-3">Acerto com produtores</h5>
						<a class="waves-effect waves-light btn" target="_blank" href="http://www.domper.com.br/site/solucoes_fruticultura.php">Saiba mais</a>
					</div>
				</li>
			</ul>
		</div>
