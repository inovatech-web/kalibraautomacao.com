<div class="row">
	<!-- Heading -->
	<div class="col xs12 m12 s12 l12">
		<div class="title-center wow bounceIn">
			<div class="bottom-line">
				<h2><?php echo Text::SERVICES; ?></h2>
				<span class="line"><em></em></span>
			</div>
		</div>
		<div class="overhide">
			<div class="description col xs12 s12">
				<p class="center-align">
				</p>
			</div>
		</div>
	</div>
	<!-- First -->
	<div class="hidden col xs12 m4 s4 l4 wow fadeInLeft" data-wow-duration="2s">
		<h5 class="center">Calibração RBC</h5>
		<p>A Kalibra está preparada para execução de serviços de calibração em balanças de todas as marcas e modelos,
			com qualidade e agilidade, na busca de atender as expectativas do cliente, assegurando a confiabilidade nos resultados apresentados.
		</p>
	</div>
	<!-- Second -->
	<div class="hidden col xs12 m4 s4 l4 wow fadeInUp" data-wow-duration="1s">
		<h5 class="center">Manutenção Corretiva</h5>
		<p>
			A manutenção corretiva é feita de acordo com a necessidade do cliente. Contamos com uma central de atendimento 24 horas que nos permite um atendimento ininterrupto, inclusive aos sábados, domingos e feriados.
		</p>
	</div>
	<!-- Third -->
	<div class="hidden col xs12 m4 s4 l4 wow fadeInRight" data-wow-duration="2s">
		<h5 class="center">Manutenção Preventiva</h5>
		<p>
			O Programa de Manutenção Preventiva é elaborado para o atendimento das reais necessidades de sua empresa. Antes da elaboração do programa, levantamos todas as informações necessárias de seus equipamentos, como por exemplo, a localização e influência dos equipamentos no processo produtivo, e com esses resultados chegaremos a um diagnóstico das reais necessidades de sua empresa. As manutenções preventivas permitem que sua empresa obtenha maior confiabilidade nas pesagens, expressivas reduções de custo, uma vez que as paradas corretivas serão diminuídas, o patrimônio é melhor preservado, aumentando a vida útil dos equipamentos.
		</p>
	</div>

	<div class="col xs12 m6 s6 l6 wow fadeInLeft" data-wow-duration="2s">
		<h5 class="center">Calibração RBC</h5>
		<p>A Kalibra está preparada para execução de serviços de calibração em balanças de todas as marcas e modelos,
			com qualidade e agilidade, na busca de atender as expectativas do cliente, assegurando a confiabilidade nos resultados apresentados.
		</p>
		<h5 class="center">Manutenção Corretiva</h5>
		<p>
			A manutenção corretiva é feita de acordo com a necessidade do cliente. Contamos com uma central de atendimento 24 horas que nos permite um atendimento ininterrupto, inclusive aos sábados, domingos e feriados.
		</p>
	</div>
	<div class="col xs12 m6 s6 l6 wow fadeInRight" data-wow-duration="2s">
		<h5 class="center">Manutenção Preventiva</h5>
		<p>
			O Programa de Manutenção Preventiva é elaborado para o atendimento das reais necessidades de sua empresa. Antes da elaboração do programa, levantamos todas as informações necessárias de seus equipamentos, como por exemplo, a localização e influência dos equipamentos no processo produtivo, e com esses resultados chegaremos a um diagnóstico das reais necessidades de sua empresa. As manutenções preventivas permitem que sua empresa obtenha maior confiabilidade nas pesagens, expressivas reduções de custo, uma vez que as paradas corretivas serão diminuídas, o patrimônio é melhor preservado, aumentando a vida útil dos equipamentos.
		</p>
	</div>
</div>
