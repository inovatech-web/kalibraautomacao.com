<div class="row">
	<!-- Heading -->
	<div class="col xs12 m12 s12 l12">
		<div class="title-center">
			<div class="bottom-line">
				<h2><?php echo Text::ABOUT; ?></h2>
				<span class="line"><em></em></span>
			</div>
		</div>
		<div class="overhide">
			<div class="description col xs12 s12">
				<p class="center-align">
				</p>
			</div>
		</div>
	</div>
	<!-- One
	<div class="col xs12 m12 s12 l12">
		<div class="slider">
			<ul class="slides">
				<li>
					<img src="http://lorempixel.com/580/250/nature/1">
					<div class="caption center-align">
						<h3>This is our big Tagline!</h3>
						<h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
					</div>
				</li>
				<li>
					<img src="http://lorempixel.com/580/250/nature/2">
					<div class="caption left-align">
						<h3>Left Aligned Caption</h3>
						<h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
					</div>
				</li>
				<li>
					<img src="http://lorempixel.com/580/250/nature/3">
					<div class="caption right-align">
						<h3>Right Aligned Caption</h3>
						<h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
					</div>
				</li>
				<li>
					<img src="http://lorempixel.com/580/250/nature/4">
					<div class="caption center-align">
						<h3>This is our big Tagline!</h3>
						<h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
					</div>
				</li>
			</ul>
		</div>
	</div>-->
	<!-- Second -->
	<div class="col xs12 m12 s12 l12 center">
		<div class="icon-block">
			<p>A Kalibra é uma empresa especializada em automação comercial. Dispõe de soluções e equipamentos para os mais variados tipos de segmentos.
				Entre os principais serviços destacam-se:
			</p>
			<ul class="list-items">
				<li class="list-item"><i class="fa fa-check"></i>Intervenções técnicas em impressoras fiscais;</li>
				<li class="list-item"><i class="fa fa-check"></i>Aferições em balanças eletrônicas</li>
				<li class="list-item"><i class="fa fa-check"></i>Assistência técnica em informática e automação;</li>
				<li class="list-item"><i class="fa fa-check"></i>Implantação, consultoria e suporte de Sistemas de Gestão.</li>
			</ul>
			<p>
				Atendimento no local e plantões nos feriados e finais de semana, sempre buscando a eficiência na prestação de nossos serviços.
			</p>
		</div>
	</div>
</div>
</div>
