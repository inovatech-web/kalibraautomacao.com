<div class="row">
	<div class="col xs12 m12 s12 l12">
		<div class="title-center">
			<div class="bottom-line">
				<h2><?php echo Text::LOCATION; ?></h2>
				<span class="line"><em></em></span>
			</div>
		</div>
		<div class="overhide">
			<div class="description col xs12 s12">
				<p class="center-align">				
				<!--
					Se você tem alguma dúvida, crítica ou sugestão, envie uma mensagem 
					Use o formulário abaixo ou para o e-mail <a href="mailto:contato@kalibraautomacao.com">contato@kalibraautomacao.com</a>.
				-->
				</p>
			</div>
		</div>
	</div>
	<div class="col xs12 m12 s12 l12 full-width">
		<div class="col s12">
			<div id="map-canvas"></div>
		</div>
	</div>
</div>