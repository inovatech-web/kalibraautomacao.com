<?php
if(isset($_POST['submit'])){
    $to = "ec.pedroescobar@gmail.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $name = $_POST['name'];
    $subject = "Contato pelo site";
    $message = $name . " Mensagem:" . "\n\n" . $_POST['message'];

		// O return-path deve ser ser o mesmo e-mail do remetente.
		$headers = "MIME-Version: 1.1\r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
		$headers .= "From: pedro@inovatechinfo.com.br\r\n"; // remetente
		$headers .= "Return-Path: pedro@inovatechinfo.com.br\r\n"; // return-path
		$headers .= "Reply-to: pedro@inovatechinfo.com.br\r\n"; // return-path
		$envio = mail("pedro@inovatechinfo.com.br", "Assunto", "Texto", $headers,"-r".$from);

		if($envio)
 			echo "Mensagem enviada com sucesso";
		else
 			echo "A mensagem não pode ser enviada";    
    }
?>
<div class="row">
	<!-- Heading -->
	<div class="col xs12 m12 s12 l12">
		<div class="title-center wow bounceIn">
			<div class="bottom-line">
				<h2><?php echo Text::CONTACT; ?></h2>
				<span class="line"><em></em></span>
			</div>
		</div>
		<div class="overhide">
			<div class="description col xs12 s12">
				<p class="center-align">
					Se você tem alguma dúvida, crítica ou sugestão, envie uma mensagem
					Use o formulário abaixo ou para o e-mail <a href="mailto:contato@kalibraautomacao.com">contato@kalibraautomacao.com</a>.
				</p>
			</div>
		</div>
	</div>
	<!-- One -->
	<div class="connect-form col xs12 m6 s6 l6 wow fadeInLeft" data-wow-duration="1s">
		<form action="" method="post">
		<div class="icon-block">
			<div class="row">
				<div class="input-field col s6">
					<?php echo FormHelper::input('Nome', 'name');?>
				</div>
				<div class="input-field col s6">
					<?php echo FormHelper::input('E-mail', 'email', 'email');?>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<?php echo FormHelper::input('Telefone', 'phone', 'tel');?>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<?php echo FormHelper::textarea('Mensagem', 'message', ['class'=>'materialize-textarea']);?>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<button type="submit" class="btn btn-large uppercase bold-600 col s12 waves-effect" name="submit" >
						Enviar
					</button>
				</div>
			</div>
		</div>
	</form>
	</div>
	<!-- Second -->
	<div class="col xs12 m6 s6 l6 wow fadeInRight" data-wow-duration="1s">
		<div class="icon-block">
			<h5 class="center"><?php echo Text::ADDRESS; ?></h5>
			<div class="center">
				<p class="description">
					<?php echo $address['rua'].', '.$address['numero']; ?><br>
					<?php echo $address['bairro'].' / '.$address['cep']; ?><br>
					<?php echo $address['cidade'].' - '.$address['uf']; ?><br>
				</p>
			</div>
		</div>
		<div class="icon-block">
			<h5 class="center"><?php echo Text::HORARIO_ATENDIMENTO; ?></h5>

			<div class="center">
				<div class="bottom-line">
					<h6>Segunda à Sexta</h6>
					<span class="line"></span>
				</div>
				<h4 class="teal-text text-darken-2">8:00 às 12:00</h4>
				<h4 class="teal-text text-darken-2">13:30 às 18:00</h4>
			</div>
		</div>
	</div>
</div>
