<?php
	$menu = "";
	$menu .= '<li><a href="#home" 		class="">'.Text::HOME.'</a></li>';
	$menu .= '<li><a href="#services" 	class="">'.Text::SERVICES.'</a></li>';
	$menu .= '<li><a href="#about"  	class="">'.Text::ABOUT.'</a></li>';
	$menu .= '<li><a href="#location" 	class="">'.Text::LOCATION.'</a></li>';
	$menu .= '<li><a href="#contact" 	class="">'.Text::CONTACT.'</a></li>';
?>
<nav role="navigation">
	<div class="nav-wrapper container">
		<a href="#home" class="brand-logo"><img src="./resources/img/logo/kalibra-150.png"></a>
		<ul class="center hide-on-med-and-down one-page-menu">
			<?php echo $menu;?>
		</ul>
		<ul id="slide-out" class="side-nav">
			<?php echo $menu;?>
		</ul>
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
	</div>
</nav>
