<?php require_once('./elements/data.php'); ?>
<?php require_once('./elements/helper.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="description" content="Automação Comercial e Balanças">
  <meta name="keywords" content=" kalibra automação balanças sistemas crc corretiva preventiva toledo filizola urano bematech">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="./resources/css/materialize.min.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="./resources/css/custom.css" media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="./resources/css/socialmedias.css" media="screen,projection"/>
	<link rel="stylesheet" href="./resources/css/partners.css" media="screen,projection">
	<link rel="stylesheet" href="./resources/css/materialize.min.css">
	<link rel="stylesheet" href="./resources/font-awesome-4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css">
	<link rel="icon" type="image/png" href="./resources/img/favicon.png" />
	<!--Let browser know website is optimized for mobile-->

	<title><?php echo $title; ?></title>
</head>
<body>
<!--
	<div id="depreload" style="background: red;" class="table">
		<div class="table-cell wrapper">
			<div class="circle">
				<canvas class="line" width="560px" height="560px"></canvas>
				<img src="./resources/img/logo.png" class="logo" alt="logo" />
			</div>
			<p class="perc"></p>
			<p class="loading">Loading</p>
		</div>
	</div>
-->
<div id="depreload" class="table" style="z-index: 99999;">
	<div class="table-cell wrapper">
		<div class="circle" >
			<canvas class="line" width="360px" height="360px" ></canvas>
			<span class="logo process-val" ></span>
		</div>
	</div>
	<span class="openLeft"></span>
	<span class="openRight"></span>
</div>
<header class="navbar-inverse valign-wrapper">
	<?php include('./elements/menu.php'); ?>
</header>
<main>
	<div id="home" class="section scrollspy">
		<?php include('./elements/pages/home.php'); ?>
	</div>
	<div id="services" class="container section scrollspy">
		<?php include('./elements/pages/services.php'); ?>
	</div>
	<div id="partners" class="container section scrollspy">
		<?php include('./elements/pages/partners.php'); ?>
	</div>
	<div id="about" class="section scrollspy">
		<?php include('./elements/pages/about.php'); ?>
	</div>
	<div id="location" class="section scrollspy">
		<?php include('./elements/pages/location.php'); ?>
	</div>
	<div id="contact" class="container section scrollspy">
		<?php include('./elements/pages/contact.php'); ?>
	</div>
</main>
<footer class="page-footer black">
	<?php include('./elements/footer.php'); ?>
</footer>
<script type="text/javascript" src="./resources/js/jquery-2.1.1.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="./resources/js/materialize.min.js"></script>
<script src="./resources/js/ca-main.js"></script>
<script src="./resources/js/maps.js"></script>
<script src="./resources/js/wow.min.js"></script>
<script src="./resources/js/jquery.DEPreLoad.js"></script>
<script src="./resources/js/loader.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var mn = $("header.navbar-inverse nav");

		$(window).scroll(function () {
			if( $(this).scrollTop() > 60 ) {
				mn.addClass("pinned");
			} else {
				mn.removeClass("pinned");
			}
		});
		new WOW().init();
	});
</script>
</body>
</html>
