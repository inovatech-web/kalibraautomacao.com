//Google Maps Settings
// When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);
function init() {

	var coord_1 = -29.1527825;
	var coord_2 = -51.219652;
	var zoom = 15;

	// Basic options for a simple Google Map
	// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	var mapOptions = {
		// How zoomed in you want the map to start at (always required)
		zoom: zoom,

		// The latitude and longitude to center the map (always required)
		center: new google.maps.LatLng(coord_1, coord_2),

		scrollwheel: false,

		// How you would like to style the map. 
		// This is where you would paste any style found on Snazzy Maps.

		styles: //Paste snazzymaps.com Styles Here
		[{"featureType":"all","elementType":"all","stylers":[{"hue":"#1c321d"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-70}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60}]}]

	};

	// Get the HTML DOM element that will contain your map 
	// We are using a div with id="map" seen below in the <body>
	var mapElement = document.getElementById("map-canvas");

	// Create the Google Map using our element and options defined above
	var map = new google.maps.Map(mapElement, mapOptions);

	// Let's also add a marker while we're at it
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(coord_1, coord_2),
		map: map
	});
};
